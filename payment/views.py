from django.shortcuts import render, redirect

from .models import Payment
from projects.models import Project, Task, TaskOffer
from projects.templatetags.project_extras import get_accepted_task_offer
from .forms import PaymentForm
from user.forms import ReviewForm
from django.contrib.auth.decorators import login_required


@login_required
def payment(request, project_id, task_id):
    task = Task.objects.get(pk=task_id)
    sender = Project.objects.get(pk=project_id).user
    receiver = get_accepted_task_offer(task).offerer

    if request.method == 'POST':
        payment = Payment(payer=sender, receiver=receiver, task=task)
        payment.save()
        task.status = Task.PAYMENT_SENT  # Set task status to payment sent
        task.save()

        return redirect('receipt', project_id=project_id, task_id=task_id)

    form = PaymentForm()

    return render(request,
                  'payment/payment.html', {
                      'form': form,
                  })


@login_required
def receipt(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    taskoffer = get_accepted_task_offer(task)

    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.reviewer = request.user.profile
            review.reviewee = taskoffer.offerer if taskoffer.offerer != request.user.profile else project.user
            review.save()

            from django.core import mail
            if review.reviewee.user.email:
                from django.contrib.sites.shortcuts import get_current_site
                current_site = get_current_site(request)
                try:
                    with mail.get_connection() as connection:
                        mail.EmailMessage(
                            "New review received! - Agreelance",
                            "Someone has reviewed you!\n"
                            "The user \"{}\" gave you a rating of {}/5 and wrote:\n\n"
                            "{}\n\n"
                            "All your reviews can be found at {}/user/{}"
                                .format(review.reviewer.user.username, review.rating,
                                        review.review, current_site.domain, review.reviewee.user.username),
                            "Agreelance",
                            [review.reviewee.user.email],
                            connection=connection,
                        ).send()
                except Exception as e:
                    from django.contrib import messages
                    messages.success(request,
                                     'Sending of email to ' + review.reviewee.user.email + " failed: " + str(e))

            return redirect('home')

    form = ReviewForm()

    return render(request,
                  'payment/receipt.html', {
                      'project': project,
                      'task': task,
                      'taskoffer': taskoffer,
                      'form': form
                  })
