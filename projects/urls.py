from django.urls import path
from .views import *

urlpatterns = [
    path('', projects.projects, name='projects'),
    path('new/', new_project.new_project, name='new_project'),
    path('<project_id>/', project_view.project_view, name='project_view'),
    path('<project_id>/tasks/<task_id>/', task_view.task_view, name='task_view'),
    path('<project_id>/tasks/<task_id>/upload/', upload_file_to_task.upload_file_to_task, name='upload_file_to_task'),
    path('<project_id>/tasks/<task_id>/permissions/', task_permissions.task_permissions, name='task_permissions'),
    path('delete_file/<file_id>', delete_file.delete_file, name='delete_file'),
]
