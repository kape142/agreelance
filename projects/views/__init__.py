__all__ = ['projects', 'new_project', 'project_view', 'task_view',
           'upload_file_to_task', 'task_permissions', 'delete_file']