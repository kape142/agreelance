from django.shortcuts import render

from projects.models import Project, ProjectCategory


def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request, 'projects/projects.html', {
        'projects': projects,
        'project_categories': project_categories,
    })
