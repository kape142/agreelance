from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render

from projects.forms import ProjectForm
from projects.models import ProjectCategory, Task
from user.models import Profile


@login_required
def new_project(request):
    if request.method == 'POST':
        return new_project_post(request)

    return render(request, 'projects/new_project.html', {'form': ProjectForm()})


def new_project_post(request):
    form = ProjectForm(request.POST)
    if form.is_valid():
        project = form.save(commit=False)
        project.user = request.user.profile
        project.category = get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
        project.save()

        notify_people(project, request)
        create_tasks(project, request)

        return redirect('project_view', project_id=project.id)
    else:
        raise ValueError("Form is not valid")


def create_tasks(project, request):
    task_title = request.POST.getlist('task_title')
    task_description = request.POST.getlist('task_description')
    task_budget = request.POST.getlist('task_budget')
    for i in range(0, len(task_title)):
        Task.objects.create(
            title=task_title[i],
            description=task_description[i],
            budget=task_budget[i],
            project=project,
        )


def notify_people(project, request):
    people = Profile.objects.filter(categories__id=project.category.id)
    for person in people:
        notify_person(person, project, request)


def notify_person(person, project, request):
    from django.contrib.sites.shortcuts import get_current_site
    current_site = get_current_site(request)
    if person.user.email:
        try:
            person.user.email_user(
                "New Project: {0}".format(project.title),
                "A new project you might be interested in was created and can be viewed at {0}/projects/{1}"
                    .format(current_site.domain, project.id),
                "Agreelance"
            )
        except Exception as e:
            from django.contrib import messages
            messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))