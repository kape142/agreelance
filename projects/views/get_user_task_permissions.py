def get_user_task_permissions(user, task):
    if user == task.project.user.user:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }

    if task.accepted_task_offer() and task.accepted_task_offer().offerer == user.profile:
        return {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }

    user_permissions = {
        'write': False,
        'read': False,
        'modify': False,
        'owner': False,
        'view_task': False,
        'upload': False,
    }

    permission_types = [
        ('read', lambda: user.profile.task_participants_read.filter(id=task.id).exists()),
        ('upload', lambda: user.profile.teams.filter(task__id=task.id, write=True).exists()),
        ('view_task', lambda: user.profile.teams.filter(task__id=task.id).exists()),
        ('write', lambda: user.profile.task_participants_write.filter(id=task.id).exists()),
        ('modify', lambda: user.profile.task_participants_modify.filter(id=task.id).exists()),
    ]

    for key, fallback in permission_types:
        set_unset_user_permission(user_permissions, key, fallback)

    return user_permissions


def set_unset_user_permission(user_permissions, key, fallback):
    if not user_permissions[key]:
        user_permissions[key] = fallback()
