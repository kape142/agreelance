from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect

from ..forms import TaskPermissionForm
from ..models import Project, Task


@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()
    if project.user == request.user.profile or user == accepted_task_offer.offerer.user:
        task = Task.objects.get(pk=task_id)
        if int(project_id) == task.project.id:
            if request.method == 'POST':
                task_permission_form = TaskPermissionForm(request.POST)
                if task_permission_form.is_valid():
                    try:
                        username = task_permission_form.cleaned_data['user']
                        user = User.objects.get(username=username)
                        permission_type = task_permission_form.cleaned_data['permission']
                        if permission_type == 'Read':
                            task.read.add(user.profile)
                        elif permission_type == 'Write':
                            task.write.add(user.profile)
                        elif permission_type == 'Modify':
                            task.modify.add(user.profile)
                    except (Exception):
                        print("user not found")
                    return redirect('task_view', project_id=project_id, task_id=task_id)

            task_permission_form = TaskPermissionForm()
            return render(
                request,
                'projects/task_permissions.html',
                {
                    'project': project,
                    'task': task,
                    'form': task_permission_form,
                }
            )
    return redirect('task_view', project_id=project_id, task_id=task_id)
