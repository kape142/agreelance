from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

from ..models import TaskFile


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
