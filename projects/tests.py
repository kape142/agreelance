from django.core.management import call_command
from django.test import TestCase, Client
from unittest import skip
from projects.models import Project, TaskOffer, Task
from projects.views.get_user_task_permissions import get_user_task_permissions
from user.models import User


# Create your tests here.

class ProjectTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'test_seed.json')

# Task 2
    def test_get_user_task_permissions(self):
        self.get_user_task_permissions_owner()
        self.get_user_task_permissions_offerer()
        self.get_user_task_permissions_other()
        self.get_user_task_permissions_other_with_1_permission()

    def get_user_task_permissions_owner(self):
        user = User.objects.get(pk=1)
        task = Task.objects.get(pk=1)
        permissions = get_user_task_permissions(user, task)
        self.assertTrue(permissions['write'])
        self.assertTrue(permissions['read'])
        self.assertTrue(permissions['modify'])
        self.assertTrue(permissions['owner'])
        self.assertTrue(permissions['upload'])

    def get_user_task_permissions_offerer(self):
        user = User.objects.get(pk=3)
        task = Task.objects.get(pk=1)
        permissions = get_user_task_permissions(user, task)
        self.assertTrue(permissions['write'])
        self.assertTrue(permissions['read'])
        self.assertTrue(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertTrue(permissions['upload'])

    def get_user_task_permissions_other(self):
        user = User.objects.get(pk=2)
        task = Task.objects.get(pk=1)
        permissions = get_user_task_permissions(user, task)
        self.assertFalse(permissions['write'])
        self.assertFalse(permissions['read'])
        self.assertFalse(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertFalse(permissions['view_task'])
        self.assertFalse(permissions['upload'])

    def get_user_task_permissions_other_with_1_permission(self):
        user = User.objects.get(pk=2)
        task = Task.objects.get(pk=2)
        permissions = get_user_task_permissions(user, task)
        self.assertFalse(permissions['write'])
        self.assertFalse(permissions['read'])
        self.assertTrue(permissions['modify'])
        self.assertFalse(permissions['owner'])
        self.assertFalse(permissions['view_task'])
        self.assertFalse(permissions['upload'])

    def test_project_view(self):
        c = Client()
        self.assertEqual(c.get("/projects/1/").status_code, 200)
        self.project_view_offer_submit()
        c.login(username='admin', password="qwerty123")
        self.project_view_offer_response(c)
        self.project_view_status_change(c)

    def project_view_offer_submit(self):
        c = Client()
        c.login(username='harrypotter', password="qwerty123")
        self.assertEqual(len(TaskOffer.objects.all()), 1)
        response = c.post("/projects/1/", {
            'title': 'title',
            'description': 'description',
            'price': 100,
            'taskvalue': 2,
            'offer_submit': True
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(TaskOffer.objects.all()), 2)

    def project_view_offer_response(self, c):
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'p')
        response = c.post("/projects/1/", {
            'status': 'a',
            'feedback': 'feedback',
            'taskofferid': 2,
            'offer_response': True
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'a')
        self.assertEqual(TaskOffer.objects.get(pk=2).feedback, 'feedback')

    def project_view_status_change(self, c):
        self.assertEqual(Project.objects.get(pk=1).status, 'o')
        response = c.post("/projects/1/", {
            'status': 'i',
            'status_change': True
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Project.objects.get(pk=1).status, 'i')

# Task 3 boundary test
    def generate_text(self, length):
        text = "something"
        while len(text) < length:
            text += text
        return text[0:length]

    def test_project_offers_boundary_above_max(self):
        with self.assertRaises(AssertionError):
            self.project_offers_boundary(title=self.generate_text(201))

    def test_project_offers_boundary_max(self):
        self.project_offers_boundary(title=self.generate_text(200))

    def test_project_offers_boundary_normal(self):
        self.project_offers_boundary(title=self.generate_text(50))

    def test_project_offers_boundary_min(self):
        self.project_offers_boundary(title='s')

    def test_project_offers_boundary_below_min(self):
        with self.assertRaises(AssertionError):
            self.project_offers_boundary(title='')

    def project_offers_boundary(self, title='title', description='description', price=100):
        data = {
            'title': title,
            'description': description,
            'price': price,
            'taskvalue': 2,
            'offer_submit': True
        }
        c = Client()
        c.login(username='harrypotter', password="qwerty123")
        self.assertEqual(len(TaskOffer.objects.all()), 1)
        response = c.post("/projects/1/", data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(TaskOffer.objects.all()), 2)

# Task 3 output coverage
    def test_project_offers_accept(self):
        self.project_view_offer_submit()
        c = Client()
        c.login(username='admin', password="qwerty123")
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'p')
        response = c.post("/projects/1/", {
            'status': 'a',
            'feedback': 'feedback test',
            'taskofferid': 2,
            'offer_response': True
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'a')
        self.assertEqual(TaskOffer.objects.get(pk=2).feedback, 'feedback test')

    @skip("bug, submitting an offer response with pending status should not be possible")
    def test_project_offers_pending(self):
        self.project_view_offer_submit()
        c = Client()
        c.login(username='admin', password="qwerty123")
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'p')
        response = c.post("/projects/1/", {
            'status': 'p',
            'feedback': 'feedback test',
            'taskofferid': 2,
            'offer_response': True
        })
        print(response)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'p')
        self.assertEqual(TaskOffer.objects.get(pk=2).feedback, '')

    def test_project_offers_decline(self):
        self.project_view_offer_submit()
        c = Client()
        c.login(username='admin', password="qwerty123")
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'p')
        response = c.post("/projects/1/", {
            'status': 'd',
            'feedback': 'feedback test',
            'taskofferid': 2,
            'offer_response': True
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TaskOffer.objects.get(pk=2).status, 'd')
        self.assertEqual(TaskOffer.objects.get(pk=2).feedback, 'feedback test')

    def test_project_offers_nonexistant(self):
        # self.project_view_offer_submit()
        c = Client()
        c.login(username='admin', password="qwerty123")
        response = c.post("/projects/1/", {
            'status': 'd',
            'feedback': 'feedback test',
            'taskofferid': 2,
            'offer_response': True
        })
        self.assertEqual(response.status_code, 404)
        with self.assertRaises(TaskOffer.DoesNotExist):
            self.assertEqual(TaskOffer.objects.get(pk=2), 'd')

    def test_create_project(self):
        self.project_view_offer_submit()
        c = Client()
        c.login(username='admin', password="qwerty123")
        c.post("/projects/new/", {
            'title': 'Test Project',
            'description': 'A test project',
            'category_id': 1,
            'task_title': ['Test Task'],
            'task_budget': [100],
            'task_description': ['A test task']
        })
        created_project = Project.objects.get(pk=2)
        self.assertEqual(created_project.title, 'Test Project')
        self.assertEqual(created_project.description, 'A test project')
        self.assertEqual(created_project.category_id, 1)

        created_task = Task.objects.get(pk=3)
        self.assertEqual(created_task.title, 'Test Task')
        self.assertEqual(created_task.budget, 100)
        self.assertEqual(created_task.description, 'A test task')
        self.assertEqual(created_task.project, created_project)

