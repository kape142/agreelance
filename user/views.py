from django.http import HttpResponse
from projects.models import ProjectCategory
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse

from .forms import SignUpForm
from .models import Review, User, Profile, Message


def index(request):
    return render(request, 'base.html')


def profile(request, username):
    reviews = Review.objects.filter(reviewee__user__username=username).order_by("-timestamp")
    message_query = (Q(sender__user__username=username) & Q(receiver__user__username=request.user.username)) | \
                    (Q(receiver__user__username=username) & Q(sender__user__username=request.user.username))
    msgs = Message.objects.filter(message_query).order_by("timestamp")
    if request.method == 'POST':
        msg = Message()
        msg.text = request.POST["text"]
        lastmsg = msgs.filter(sender__user__username=request.user.username).last()
        if not lastmsg or msg.text != lastmsg.text:
            msg.receiver = Profile.objects.filter(user__username=username).first()
            msg.sender = Profile.objects.filter(user__username=request.user.username).first()
            msg.save()
            if msg.receiver.user.email:
                from django.contrib.sites.shortcuts import get_current_site
                current_site = get_current_site(request)
                try:
                    msg.receiver.user.email_user(
                        "New message received! - Agreelance",
                        "Someone has sent you a message!\n"
                        "The user \"{}\" wrote:\n\n"
                        "{}\n\n"
                        "All your messaging with this person can be found at {}/user/{}"
                        .format(msg.sender.user.username,
                                msg.text, current_site.domain, msg.sender.user.username),
                        "Agreelance"
                    )
                except Exception as e:
                    from django.contrib import messages
                    messages.success(request, 'Sending of email to ' + msg.receiver.user.email + " failed: " + str(e))
            return redirect("profile", username)
    return render(request, 'user/profile.html', {
        'showmessages': request.user.username and username != request.user.username,
        'username': username,
        'reviews': reviews,
        'msgs': msgs
    })


def signup(request):
    if request.method == 'POST':
        try:
            form = SignUpForm(request.POST)
            if form.is_valid():
                user = form.save()
                response = request.POST['g-recaptcha-response']
                user.refresh_from_db()
                import requests
                r = requests.post("https://www.google.com/recaptcha/api/siteverify", {
                    'secret': "6Le1SOEUAAAAAMJt7cdFmgCmfGMvO-NDKXlvSo9Q",
                    'response': response
                })
                check = r.json()
                print(check)
                from django.contrib import messages
                if not check["success"]:
                    messages.error(request, 'The reCAPTCHA verification failed. Make sure you are not a bot and try again.')
                    return redirect('signup')

                user.profile.company = form.cleaned_data.get('company')

                user.is_active = False
                user.profile.categories.add(*form.cleaned_data['categories'])
                user.save()
                messages.success(request, 'Your account has been created and is awaiting verification.')
                return redirect('home')
        except Exception as e:
            print("error: {}".format(e))


    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})
