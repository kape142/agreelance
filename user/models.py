from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    company = models.TextField(max_length=50, blank=True)
    phone_number = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    state = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    postal_code = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)
    categories = models.ManyToManyField('projects.ProjectCategory', related_name='competance_categories')

    def __str__(self):
        return "Profile: {}".format(self.user.username)


class Review(models.Model):
    reviewer = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='review_writing_user')
    reviewee = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='review_receiving_user')
    rating = models.IntegerField(blank=True)
    review = models.TextField(max_length=500, blank=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "by: {0}, of: {1}, rating: {2}/5, review: {3}, time: {4}".format(
            self.reviewer.user.username,
            self.reviewee.user.username,
            self.rating, self.review, self.timestamp)


class Message(models.Model):
    sender = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='message_sending_user')
    receiver = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='message_sending_receiver')
    text = models.TextField(max_length=500, blank=True)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "from: {0}, to: {1}, text: {2}, time: {3}".format(
            self.sender.user.username,
            self.receiver.user.username,
            self.text, self.timestamp)

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save() # Saves the user profile
