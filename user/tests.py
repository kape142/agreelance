from collections import OrderedDict
from django.test import TestCase, Client
from django.core.management import call_command
from projects.models import ProjectCategory
from user.forms import SignUpForm
from user.models import Review, Message
import random
from allpairspy import AllPairs
from unittest import skip

# Create your tests here.

# Task 3 boundary test
class UserTestCase(TestCase):
    def setUp(self):
        call_command('loaddata', 'test_seed.json')
        self.max_length = {
            'username': 150,
            'first_name': 30,
            'last_name': 30,
            'company': 30,
            'email': 244, # 254 -10 for @gmail.com
            'password': 50,
            'phone_number': 50,
            'country': 50,
            'state': 50,
            'city': 50,
            'postal_code': 50,
            'street_address': 50,
        }
        self.min_length = {
            'username': 1,
            'first_name': 1,
            'last_name': 1,
            'company': 1,
            'email': 1,
            'password': 8,
            'phone_number': 1,
            'country': 1,
            'state': 1,
            'city': 1,
            'postal_code': 1,
            'street_address': 1,
        }
        self.categories = list(ProjectCategory.objects.all())
        self.symbols = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

    def convert(self, data):
        newData = {}
        for item in data.items():
            if item[0] == 'email':
                if len(item[1]) > 0 and not item[1] == 'nosuffixemail':
                    newData['email'] = str(item[1])+"@gmail.com"
                    if item[1] == 'differentemail':
                        newData['email_confirmation'] = self.generate_text(25) + "@gmail.com"
                    else:
                        newData['email_confirmation'] = str(item[1])+"@gmail.com"
                else:
                    newData['email'] = item[1]
                    newData['email_confirmation'] = item[1]
            elif item[0] == 'password':
                newData['password1'] = item[1]
                if item[1] == 'Differentpassword1':
                    newData['password2'] = self.generate_text(20)
                else:
                    newData['password2'] = item[1]
            else:
                newData[item[0]] = item[1]
        newData['categories'] = self.categories
        return newData

    def generate_text(self, length):
        size = round(length)
        text = ''
        while len(text) < size:
            text += random.choice(self.symbols)
        return text

    def test_signup_boundary_above_max(self):
        for i in range(len(self.max_length)):
            if i == 5:
                continue # skip password as there is no upper bound defined
            data = {}
            j = i
            for (name, value) in self.max_length.items():
                size = value+1 if j == 0 else value/2
                data[name] = self.generate_text(size)
                j -= 1
            form = SignUpForm(self.convert(data))
            self.assertFalse(form.is_valid())

    def test_signup_boundary_max(self):
        for i in range(len(self.max_length)):
            data = {}
            j = i
            for (name, value) in self.max_length.items():
                size = value if j == 0 else value/2
                data[name] = self.generate_text(size)
                j -= 1
            form = SignUpForm(self.convert(data))
            self.assertTrue(form.is_valid())

    def test_signup_boundary_normal(self):
        for i in range(len(self.max_length)):
            data = {}
            j = i
            for (name, value) in self.max_length.items():
                size = value/2
                data[name] = self.generate_text(size)
                j -= 1
            form = SignUpForm(self.convert(data))
            self.assertTrue(form.is_valid())

    def test_signup_boundary_min(self):
        for i in range(len(self.min_length)):
            data = {}
            j = i
            for (name, value) in self.min_length.items():
                size = value if j == 0 else self.max_length[name]/2
                data[name] = self.generate_text(size)
                j -= 1
            form = SignUpForm(self.convert(data))
            self.assertTrue(form.is_valid())

    def test_signup_boundary_below_min(self):
        for i in range(len(self.min_length)):
            if i == 3:
                continue # skip company as it is not required
            data = {}
            j = i
            for (name, value) in self.min_length.items():
                size = value-1 if j == 0 else self.max_length[name]/2
                data[name] = self.generate_text(size)
                j -= 1
            form = SignUpForm(self.convert(data))
            self.assertFalse(form.is_valid())

# Task 3 2-way domain
    def test_signup_two_way_domain(self):
        illegal_combination_dict = OrderedDict({
            'username': ['', self.generate_text(50)+'*!¤!"/(#', self.generate_text(151)],
            'first_name': ['', self.generate_text(40)],
            'last_name': ['', self.generate_text(40)],
            'company': ['', self.generate_text(40)],
            'email': ['', self.generate_text(250), 'nosuffixemail', 'differentemail'],
            'password': [self.generate_text(4), 12345678, 'Differentpassword1'],
            'phone_number': ['', self.generate_text(60)],
            'country': ['', self.generate_text(60)],
            'state': ['', self.generate_text(60)],
            'city': ['', self.generate_text(60)],
            'postal_code': ['', self.generate_text(60)],
            'street_address': ['', self.generate_text(60)],
        })
        a = AllPairs(illegal_combination_dict)
        for _, item in enumerate(a):
            data = {}
            for name, value in item._asdict().items():
                data[name] = value
            data = self.convert(data)
            form = SignUpForm(data)
            self.assertFalse(form.is_valid())

    @skip('bug, different emails should not be accepted')
    def test_signup_different_email(self):
        data = {}
        for (name, value) in self.max_length.items():
            size = value/2
            data[name] = self.generate_text(size)
        convdata = self.convert(data)
        convdata['email'] = convdata['email'][3:]
        form = SignUpForm(convdata)
        self.assertFalse(form.is_valid())

    def test_signup_different_password(self):
        data = {}
        for (name, value) in self.max_length.items():
            size = value/2
            data[name] = self.generate_text(size)
        convdata = self.convert(data)
        convdata['password2'] = convdata['password2'][3:]
        form = SignUpForm(convdata)
        self.assertFalse(form.is_valid())

# Task 3 integration
    def test_review_integration(self):
        c = Client()
        c.login(username='admin', password="qwerty123")
        c2 = Client()

        self.assertEqual(len(Review.objects.all()), 0)

        review_text = self.generate_text(30)
        rating = random.randint(1,5)

        c.post("/payment/1/1/receipt/", {
            'rating': rating,
            'review': review_text
        })
        self.assertEqual(len(Review.objects.all()), 1)
        review = Review.objects.get(pk=1)
        self.assertEqual(review.rating, rating)
        self.assertEqual(review.review, review_text)

        response = c2.get('/user/joe/')
        review2 = response.context['reviews'][0]
        self.assertEqual(review2.rating, rating)
        self.assertEqual(review2.review, review_text)


    def test_messages_integration(self):
        c = Client()
        c.login(username='admin', password="qwerty123")
        c2 = Client()
        c2.login(username='joe', password='qwerty123')
        c3 = Client()

        self.assertEqual(len(Message.objects.all()), 0)

        text = self.generate_text(30)

        c.post("/user/joe/", {
            'text': text
        })
        self.assertEqual(len(Message.objects.all()), 1)
        msg = Message.objects.get(pk=1)
        self.assertEqual(msg.text, text)

        response = c2.get('/user/admin/')
        msg2 = response.context['msgs'][0]
        self.assertEqual(msg2.text, text)

        response = c3.get('/user/admin/')
        self.assertEqual(len(response.context['msgs']), 0)



